import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BasicAccess } from '../components/model/BasicAccess';
import { EncuestaDTO } from '../components/model/EncuestaDTO';
import { EncuestaReponse } from '../components/model/EncuestaResponse';
import { HOST_BACKEND, REFRESH_TOKEN_NAME } from '../shared/constants';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  urlGuardarEncuesta: string = `${HOST_BACKEND}/api/encuesta`;

  constructor(private http: HttpClient) { }

  guardarEncuesta(encuestaDTO:EncuestaDTO){
    return this.http.post(this.urlGuardarEncuesta, encuestaDTO);
  }

  public findAll() {

      return  this.http.get<EncuestaReponse>( `${this.urlGuardarEncuesta}`)
        }


}
