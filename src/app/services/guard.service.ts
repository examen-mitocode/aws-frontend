import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { TOKEN_NAME } from '../shared/constants';
import { SecurityService } from './security.service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {

  
  constructor(
    private router: Router,
    private securityService: SecurityService) { }
    
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let token = sessionStorage.getItem(TOKEN_NAME);
    let isAdministrador = this.securityService.esRoleAdmin();

    if (token != null) {
      
      if(isAdministrador && state.url=='/encuesta/listado'){
          return true;
      }else if(isAdministrador && state.url!='/encuesta/listado'){
        this.router.navigate(['encuesta','listado']);
      }else if(!isAdministrador && state.url=="/encuesta")
        return true;
      else if(!isAdministrador && state.url!="/encuesta")
      this.router.navigate(['encuesta']);


      
      
    } else {
      if(state.url !== '/login' ){
        this.router.navigate(['login']);
      }else{
        return true;
      }
    }
    return false;
  }
}
