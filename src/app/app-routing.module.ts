import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { ListadoComponent } from './components/listado/listado.component';
import { ErrorComponent } from './components/login/error/error.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { GuardGuard } from './services/guard.service';

const routes: Routes =  [
  { path:'login',component:LoginComponent,canActivate:[GuardGuard]},
  { path:'logout',component:LogoutComponent},
  {path:'encuesta',component:EncuestaComponent, 
    children:[
      {path:"listado", component:ListadoComponent}]
      ,canActivate:[GuardGuard]},
  {path:"error",component:ErrorComponent},
  {path: '**', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
