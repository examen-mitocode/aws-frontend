import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { ErrorComponent } from './components/login/error/error.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ListadoComponent } from './components/listado/listado.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EncuestaComponent,
    NotFoundComponent,
    ErrorComponent,
    LogoutComponent,
    ListadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    { provide: LocationStrategy, useClass: PathLocationStrategy }],  bootstrap: [AppComponent]
})
export class AppModule { }
