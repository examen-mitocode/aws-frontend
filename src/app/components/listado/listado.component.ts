import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { EncuestaDTO } from '../model/EncuestaDTO';
import { EncuestaReponse } from '../model/EncuestaResponse';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  encuestas:EncuestaReponse;

  constructor(private encuestaService:EncuestaService) {
    this.findAll();
   }

  ngOnInit(): void {
  }

  findAll() {
    this.encuestaService
      .findAll()
      .subscribe((data:EncuestaReponse) => {
        if (data) 
        this.encuestas = data;
      });
  }

}
