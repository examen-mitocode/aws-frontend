import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { EncuestaDTO } from '../model/EncuestaDTO';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  encuesta:EncuestaDTO;

  constructor(private encuestaService:EncuestaService,
    public route:ActivatedRoute) { 
    this.encuesta = new EncuestaDTO();
  }

  ngOnInit(): void {
  }

  guardar(){
    this.encuestaService.guardarEncuesta(this.encuesta).subscribe((data: any)=>{
      console.log(JSON.stringify(data));
      alert("Guardado correctamente");
    }, (error)=>{
      console.error(error);
    });
  }
}
