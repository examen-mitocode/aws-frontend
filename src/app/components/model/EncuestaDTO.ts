export class EncuestaDTO{

    nombres: string;
    apellidos: string;
    edad: number;
    profesion: string;
    lugarDeTrabajo: string;
    resultadoEncuesta: string;

    constructor(){}
}