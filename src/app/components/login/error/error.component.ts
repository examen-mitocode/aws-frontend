import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SecurityService } from 'src/app/services/security.service';
import { ChangePassword } from '../../model/ChangePassword';
import { RenewPasswordFirst } from '../../model/RenewPasswordFirst';
import { RespuestaApi } from '../../model/RespuestaApi';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  password: string;
  datosRenewPassword: RenewPasswordFirst;
  datosChangePassword: ChangePassword;
  data: any={};

  constructor(
    private securityService: SecurityService,
    private route: ActivatedRoute,
  ) {
    this.route
    .queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.data.usuario =params['usuario'];
      this.data.sessionId =params['sessionId'];
      this.data.status =params['status'];

    });
    this.datosRenewPassword = new RenewPasswordFirst();
    this.datosChangePassword = new ChangePassword();
   }

  ngOnInit() {

  }

  onSubmit(status: string){


    this.datosRenewPassword.username = this.data.usuario;
    this.datosRenewPassword.password = this.password;
    this.datosRenewPassword.session = this.data.sessionId;

    if(status == 'OK-UPDATE'){
      this.securityService.renewPasswordFirst(this.datosRenewPassword).subscribe((data: RespuestaApi)=>{
        if(data.status == "OK"){
          this.data.status = "OK";
          this.data.error = "Ahora intente ingresar con su nueva clave (Cierre esta ventana)";
        }else{
          this.data.status = "ERROR";
        this.data.error = "No se pudo actualizar su clave, intente luego";
        }
        
      }, (error) => {
        this.data.status = "ERROR";
        this.data.error = "Ocurrio un Error al tratar de actualizar su nueva clave, intente luego";
      });
    }else if(status == 'OK-RESET'){
      this.datosChangePassword.username = this.data.usuario;

      this.securityService.updatePassword(this.datosChangePassword).subscribe((data: RespuestaApi) => {
        if(data.status == "OK"){
          this.data.status = "OK";
          this.data.error = "Ahora intente ingresar con su nueva clave (Cierre esta ventana)";
        }else{
          this.data.status = "ERROR";
          this.data.error = "No se pudo actualizar su clave, intente luego";
        }
      }, (error) => {
        this.data.status = "ERROR";
        this.data.error = "Ocurrio un Error al tratar de cambiar su nueva clave, intente luego";
      });
    }
}

}
