import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from '../../services/security.service';
import { ACCESS_TOKEN_NAME, PARAM_USUARIO, REFRESH_TOKEN_NAME, TOKEN_NAME } from 'src/app/shared/constants';
import { LoginDTO } from '../model/LoginDTO';
import { RespuestaApi } from '../model/RespuestaApi';
import { ErrorComponent } from './error/error.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {


  login: LoginDTO;
  
  constructor(
    private router: Router,
    private securityService: SecurityService){
      this.login = new LoginDTO();
  }
  

  doLogin():void {
    this.securityService.login(this.login).subscribe((data: RespuestaApi)=>{
      if(data.status == 'OK'){
        sessionStorage.setItem(TOKEN_NAME, data.idToken);
        sessionStorage.setItem(REFRESH_TOKEN_NAME, data.refreshToken);
        sessionStorage.setItem(ACCESS_TOKEN_NAME, data.accessToken);

        this.securityService.validarToken().subscribe((dato: any)=>{
          sessionStorage.setItem(PARAM_USUARIO, JSON.stringify(dato.body));
          this.router.navigate(["encuesta"]);
        });
      }else{

        this.router.navigate(['/error'], { queryParams: { sessionId: data.sessionId, usuario: this.login.username, status: data.status} });
      }
  }, (error) => {

    console.error("error");
    });
  }

}
